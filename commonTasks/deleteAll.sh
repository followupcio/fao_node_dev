#Stop All containers and remove them
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
#Remove all images
docker rmi $(docker images -a -q)

#Remove all volumes
docker volume rm $(docker volume ls -q --filter dangling=true)