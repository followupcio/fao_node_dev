#!/usr/bin/env bash
if [ "$INSTALL_WEBPACK" = "true" ];then
	echo '############################'
	echo 	'Installing FRONTEND dependencies'
	echo '############################'
	cd /var/www/ && \
	   npm cache clear && \
	   npm i && \
	   npm cache clear 
	echo '############################'
	echo 	'Running the App'
	echo '############################'
	npm start
else
	echo '############################'
	echo 	'Installing BACKEND dependencies'
	echo '############################'
	cd /var/www/ && \
	   npm cache clear && \
	   npm install && \
	   npm cache clear 
	export NODE_ENV=production
	echo '############################'
	echo 	'Running the App'
	echo '############################'
	nodemon -L .
fi